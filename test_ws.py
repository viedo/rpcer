from src import *


def cb(msg):
    for k, v in msg.items():
        print(f'#{k}: {v}#')
    msg['v'] *= 1.5
    return msg


if __name__ == '__main__':
    s = Wser()
    s.start_server(callback=cb)

    c = Wser()
    m = {
        'a': 'abc',
        'v': 44.54,
    }
    c.client.create_conn('se', 'localhost', s.port)
    r = c.send('se', m)
    print(r)

    while True:
        pass